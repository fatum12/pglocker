# pgLocker

Simple distributed lock implementation for Go using Postgresql.

## Usage

```sql
create table locks (
	name varchar(255) primary key,
	token varchar(255) not null,
	created_at timestamptz not null default now(),
	expire_at timestamptz
);
```

```go
func main() {
    db, err := sql.Open("postgres", "dbname=postgres user=postgres password=postgres")
    if err != nil {
        log.Fatalln(err)
    }
    
    locker := pglocker.New(db, "locks")
    lock, err := locker.Lock("lock_name", 10*time.Minute)
    if err == pglocker.ErrLockExists {
        log.Println("lock exists")
        return
    } else if err != nil {
        log.Fatalln(err)
    }
    defer lock.Release()

    // ...
}
```