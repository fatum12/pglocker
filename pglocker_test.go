package pglocker

import (
	"database/sql"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

const testTable = "locks"
const schema = `
create table if not exists ` + testTable + ` (
	name varchar(255) primary key,
	token varchar(255) not null,
	created_at timestamptz not null default now(),
	expire_at timestamptz
);
`

func TestLock(t *testing.T) {
	db := prepareDB(t)
	defer db.Close()
	locker := New(db, testTable)

	lock1, err := locker.Lock("test_lock1", 10*time.Minute)
	assert.NoError(t, err)
	lock2, err := locker.Lock("test_lock2", 10*time.Minute)
	assert.NoError(t, err)

	if assert.NotNil(t, lock1) {
		err = lock1.Release()
		assert.NoError(t, err)
	}
	if assert.NotNil(t, lock2) {
		err = lock2.Release()
		assert.NoError(t, err)
	}
}

func TestLockExists(t *testing.T) {
	db := prepareDB(t)
	defer db.Close()
	locker := New(db, testTable)
	_, err := locker.Lock("test_lock", 10*time.Minute)
	assert.NoError(t, err)

	_, err = locker.Lock("test_lock", 1*time.Minute)
	assert.Equal(t, ErrLockExists, err)
}

func TestRelease(t *testing.T) {
	db := prepareDB(t)
	defer db.Close()
	locker := New(db, testTable)

	lock, err := locker.Lock("test_lock", 10*time.Minute)
	assert.NoError(t, err)
	if assert.NotNil(t, lock) {
		err = lock.Release()
		assert.NoError(t, err)

		_, err = locker.Lock("test_lock", 20*time.Minute)
		assert.NoError(t, err)
	}
}

func prepareDB(t *testing.T) *sql.DB {
	db, err := sql.Open("postgres", "host=127.0.0.1 port=15432 dbname=postgres user=postgres password=postgres sslmode=disable connect_timeout=5")
	if err != nil {
		t.Fatal(err)
	}
	if _, err = db.Exec(schema); err != nil {
		t.Fatal(err)
	}
	if _, err = db.Exec("truncate table " + testTable); err != nil {
		t.Fatal(err)
	}
	return db
}
