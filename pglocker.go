package pglocker

import (
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"errors"
	"time"
)

var (
	ErrLockExists = errors.New("pglocker: lock exists")
	ErrWrongLock  = errors.New("pglocker: wrong lock")
)

type Locker struct {
	db    *sql.DB
	table string
}

func New(db *sql.DB, tableName string) *Locker {
	return &Locker{
		db:    db,
		table: tableName,
	}
}

func (l *Locker) Lock(name string, ttl time.Duration) (*Lock, error) {
	token, err := generateToken()
	if err != nil {
		return nil, err
	}

	tx, err := l.db.Begin()
	if err != nil {
		return nil, err
	}
	if _, err = tx.Exec("lock " + l.table + " in share row exclusive mode"); err != nil {
		tx.Rollback()
		return nil, err
	}
	res, err := tx.Exec(`
insert into `+l.table+` (name, token, expire_at)
select $1::varchar, $2, now() + $3::interval
where not exists (select 1 from `+l.table+` where name = $1 and expire_at > now())
on conflict (name) do update set token = EXCLUDED.token, expire_at = EXCLUDED.expire_at, created_at = now()
	`, name, token, ttl.Seconds())
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	if err = tx.Commit(); err != nil {
		return nil, err
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}
	if affected != 1 {
		return nil, ErrLockExists
	}

	lock := &Lock{
		db:    l.db,
		table: l.table,
		name:  name,
		token: token,
	}
	return lock, nil
}

type Lock struct {
	db    *sql.DB
	table string
	name  string
	token string
}

func (l *Lock) Release() error {
	res, err := l.db.Exec("delete from "+l.table+" where name = $1 and token = $2", l.name, l.token)
	if err != nil {
		return err
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if affected != 1 {
		return ErrWrongLock
	}
	return nil
}

func (l *Lock) ReleaseAttempts(attempts int, delay time.Duration) error {
	var err error
	for i := 0; i < attempts; i++ {
		if i > 0 {
			time.Sleep(time.Duration(i) * delay)
		}
		err = l.Release()
		if err == nil || err == ErrWrongLock {
			break
		}
	}
	return err
}

func (l *Lock) Extend(ttl time.Duration) error {
	res, err := l.db.Exec("update "+l.table+" set expire_at = now() + $1::interval where name = $2 and token = $3", ttl.Seconds(), l.name, l.token)
	if err != nil {
		return err
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if affected != 1 {
		return ErrWrongLock
	}
	return nil
}

func generateToken() (string, error) {
	token := make([]byte, 16)
	if _, err := rand.Read(token); err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(token), nil
}
